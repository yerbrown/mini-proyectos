﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomWindow : EditorWindow
{
    public List<string> debugList = new List<string>();

    string passworToEdit = "My Password";
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;
    bool hideToogle;
    int hider;

    bool showPassword = false;

    //Add menu item named "My Window" to the Window menu
    [MenuItem("Window/My Window")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        GetWindow(typeof(CustomWindow));
    }
    private void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        myString = EditorGUILayout.TextField("Text Field", myString);
        EditorGUILayout.Space(10);

        //This sections creates a  block of variables that are enabled/disabled depending on the toggle
        groupEnabled = EditorGUILayout.BeginToggleGroup("optional Settings", groupEnabled);
        myBool = EditorGUILayout.Toggle("Toogle", myBool);
        myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        EditorGUILayout.EndToggleGroup();
        EditorGUILayout.Space(10);

        //This instead shows/hides then
        hideToogle = GUILayout.Toggle(hideToogle, "Hide Toggle");

        if (hideToogle)
        {
            hider = EditorGUILayout.IntSlider("Hider field:", hider, 1, 100);
        }
        EditorGUILayout.Space(10);

        //This adds a button to do wathever we put in the functions
        GUILayout.Label("Debug the selected element names", EditorStyles.boldLabel);

        if (GUILayout.Button("Debug!"))
        {
            DebugInConsole();
        }
        EditorGUILayout.Space(10);
        EditorGUILayout.Toggle("Show Password", showPassword);
        if (showPassword)
        {
            passworToEdit = EditorGUILayout.TextField("My Password", passworToEdit);
        }
        else
        {
            //Make a password field that modifies passwordToEdit
            passworToEdit = GUILayout.PasswordField(passworToEdit, "a"[0], 25);

        }

        GUILayout.Label("Show the password", EditorStyles.boldLabel);

        //This is just another button
        if (GUILayout.Button("Show!"))
        {
            DebugPassword();
        }
    }
    private void DebugInConsole()
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            Debug.Log(obj.name);
        }
    }

    private void DebugPassword()
    {
        showPassword = !showPassword;
    }
}
