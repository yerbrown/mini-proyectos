﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(VariablesForEditorustom))]
[CanEditMultipleObjects]
public class CustomeditorPrueba : Editor
{
    SerializedProperty variablesFor;

    float thumbnailWith = 70;
    float thumbnailHeight = 70;

    private void OnEnable()
    {
        variablesFor = serializedObject.FindProperty("numberToCheck");
    }

    ///Custom inspector with conditions
    //public override void OnInspectorGUI()
    //{
    //    VariablesForEditorustom variablesForScript = (VariablesForEditorustom)target;

    //    variablesForScript.numberToCheck = EditorGUILayout.FloatField("numerino", variablesForScript.numberToCheck);
    //    EditorGUILayout.LabelField(NumberChecked());
    //}

    ///Custom inspector adding values to deffault
    //public override void OnInspectorGUI()
    //{
    //    DrawDefaultInspector();

    //    EditorGUILayout.HelpBox(NumberChecked(), MessageType.Info);
    //}


    ///Custom inspector with a button(or multiple buttons)
    //public override void OnInspectorGUI()
    //{
    //    DrawDefaultInspector();
    //    VariablesForEditorustom variablesForScript = (VariablesForEditorustom)target;
    //    if (GUILayout.Button("One more"))
    //    {
    //        variablesForScript.AddOne();
    //    }

    //    if (GUILayout.Button("One Less"))
    //    {
    //        variablesForScript.SubstactOne();
    //    }


    //}

    ///Custom inspector with buttons showing a texture
    //public override void OnInspectorGUI()
    //{
    //    DrawDefaultInspector();
    //    VariablesForEditorustom variablesForScript = (VariablesForEditorustom)target;

    //    //Con esto hacemos que los elementos se coloquen en hotizontal (si caben)
    //    GUILayout.BeginHorizontal();
    //    if (GUILayout.Button(Resources.Load<Texture>("Plus"), GUILayout.Width(thumbnailWith), GUILayout.Height(thumbnailHeight)))
    //    {
    //        variablesForScript.AddOne();
    //    }
    //    if (GUILayout.Button(Resources.Load<Texture>("Minus"), GUILayout.Width(thumbnailWith), GUILayout.Height(thumbnailHeight)))
    //    {
    //        variablesForScript.SubstactOne();
    //    }
    //    GUILayout.EndHorizontal();
    //}

    ///Muestra/oculta variables en base a las condiciones definidas
    public override void OnInspectorGUI()
    {
        VariablesForEditorustom variablesForScript = (VariablesForEditorustom)target;

        variablesForScript.hideToggle = GUILayout.Toggle(variablesForScript.hideToggle, "hide Toggle");

        if (variablesForScript.hideToggle)
        {
            variablesForScript.hider = EditorGUILayout.IntSlider("Hider field:", variablesForScript.hider, 1, 10);
        }
    }

    public string NumberChecked()
    {
        if (variablesFor.floatValue > 0)
        {
            return "El numero es positivo";
        }
        else if (variablesFor.floatValue < 0)
        {
            return "El numero es negativo";
        }
        else
        {
            return "El numero es 0";
        }
    }
}
