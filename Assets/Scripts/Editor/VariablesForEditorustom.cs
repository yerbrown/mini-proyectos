﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesForEditorustom : MonoBehaviour
{
    [ContextMenuItem("Reset Number", nameof(ResetNumber))]
    public float numberToCheck;

    [Header("Hide the variable below this")]
    public bool hideToggle;
    public int hider;

    public void AddOne()
    {
        numberToCheck += 1;
    }

    public void SubstactOne()
    {
        numberToCheck += 1;
    }

    [ContextMenu("Reset Number")]
    private void ResetNumber()
    {
        numberToCheck = 0;
    }


}
