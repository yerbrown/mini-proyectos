﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    public Transform target;
    public float moveSpeed, rotateSpeed;
    public Transform offset;
    // Start is called before the first frame update

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.LookAt(target.position);
        transform.position = Vector3.Slerp(transform.position, offset.position, moveSpeed * Time.deltaTime);
    }
}
