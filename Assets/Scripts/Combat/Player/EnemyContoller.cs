﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContoller : MonoBehaviour
{
    public float hitPower;
    public Animator anim;
    public int life;
    private void Start()
    {
        InvokeRepeating(nameof(ChangeIdleAnim), 0, 5);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Weapon"))
        {
            Vector3 otherY = new Vector3(other.transform.position.x, transform.position.y, other.transform.position.z);
            Vector3 dir = (transform.position - otherY).normalized;
            GetComponent<Rigidbody>().AddForce(dir * hitPower, ForceMode.Impulse);
            Debug.Log("Hit!");
            life -= 10;
            if (life <= 0)
            {
                CameraPlayerController.cameraPlayerControllerInstance.enemyTarget = null;
                CameraPlayerController.cameraPlayerControllerInstance.arrow.SetActive(false);
                anim.SetBool("Death", true);
            }
            else
            {

                anim.SetTrigger("Hit");
            }
        }
    }

    public void ChangeIdleAnim()
    {
        anim.SetInteger("Idle", Random.Range(1, 5));
    }
}
