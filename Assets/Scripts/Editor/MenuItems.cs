﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MenuItems
{
    //Añadir un nuevo menu al editor
    [MenuItem("Tools/Clear PlayerPrefs")]
    private static void NewMenuOption()
    {
        PlayerPrefs.DeleteAll();
    }
    //Añadir un nuevo objeto a un menu ya existente
    [MenuItem("Window/New Option")]
    private static void AnotherMenuOption()
    {

    }
    //Añadir un nuevo objeto a un menu ya existente con un sub-menu
    [MenuItem("Tools/SubMenu/Option")]
    private static void NewNestedOption()
    {

    }
    //Añadir un nuevo objeto a un menu con el atajo CTRL-SHIFT_A
    [MenuItem("Tools/New Option %#a")]
    private static void NewMenuOptionWithKey()
    {

    }
    //Añadir un nuevo objeto a un menu con el atajo CTRL-G
    [MenuItem("Tools/Item %g")]
    private static void NewnestedOptionWithKey()
    {

    }
    //Añadir un nuevo objeto a un menu con el atajo G
    [MenuItem("Tools/item2 _g")]
    private static void NewMenuOptionWithHotKey()
    {

    }
}
