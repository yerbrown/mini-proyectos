﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerController : MonoBehaviour
{
    public static CameraPlayerController cameraPlayerControllerInstance;
    public Transform target, enemyTarget;
    public float moveSpeed, rotateSpeed;
    public Transform offsetPos;
    public float offsetZ, offsetY;
    public Vector3 lerpPos;
    public Transform targetLerpPos;

    public GameObject arrow;

    public float mouseX, mouseY;
    public float mouseYMult;
    public float minY, maxY;

    private void Awake()
    {
        cameraPlayerControllerInstance = this;
    }
    // Start is called before the first frame update
    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    private void Update()
    {
        ThrowRay();
    }
    void LateUpdate()
    {
        CamControll();
        ClampEnemyTargetArrow();
    }

    public void CamControll()
    {
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y")*mouseYMult;
        mouseY = Mathf.Clamp(mouseY, -30, 60);
        target.GetComponent<PaladinContoller>().anim.SetFloat("RightTurn", mouseX);

        if (enemyTarget != null)
        {
            lerpPos = enemyTarget.position + Vector3.up * offsetY;
            targetLerpPos.position = new Vector3(targetLerpPos.position.x, target.position.y, targetLerpPos.position.z);
            target.LookAt(targetLerpPos);
        }
        else
        {
            target.Rotate(new Vector3(0, mouseX, 0));
            Vector3 lerpPosXZ = (target.position + target.forward * offsetZ) + Vector3.up * offsetY;
            lerpPos = new Vector3(lerpPosXZ.x, targetLerpPos.position.y, lerpPosXZ.z);
            lerpPos.y += mouseY;
            lerpPos.y = Mathf.Clamp(lerpPos.y, target.position.y + minY, target.position.y + maxY);
        }
        Vector3 offsetPosY = offsetPos.position;
        offsetPosY.y -= mouseY;
        offsetPosY.y = Mathf.Clamp(offsetPosY.y, target.position.y + minY, target.position.y + maxY);
        offsetPos.position = offsetPosY;
        targetLerpPos.position = lerpPos;

        transform.position = offsetPos.position;

        transform.LookAt(targetLerpPos);
    }

    public void ThrowRay()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100.0f))
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                Debug.Log("Enemyyyyyyy");
                if (Input.GetButtonDown("Fire3"))
                {
                    if (enemyTarget == null)
                    {
                        enemyTarget = hit.collider.gameObject.transform;
                        arrow.SetActive(true);
                    }
                    else
                    {
                        enemyTarget = null;
                        arrow.SetActive(false);
                    }
                }
            }
        }
    }

    public void ClampEnemyTargetArrow()
    {
        if (enemyTarget != null)
        {
            arrow.transform.LookAt(transform);
            arrow.transform.position = enemyTarget.transform.position + Vector3.up * 2;
        }
    }
}