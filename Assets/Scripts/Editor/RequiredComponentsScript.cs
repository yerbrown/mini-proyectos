﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class RequiredComponentsScript : MonoBehaviour
{
    public Rigidbody exampleRB;

    private void Reset()
    {
        exampleRB = GetComponent<Rigidbody>();
    }
}
