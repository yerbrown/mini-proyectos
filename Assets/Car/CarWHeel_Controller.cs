﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarWHeel_Controller : MonoBehaviour
{
    public Rigidbody rb;
    public float torqueForce;
    public List<GameObject> wheels = new List<GameObject>();
    private void Update()
    {
        InputsManager();
    }

    public void InputsManager()
    {
        TorqueWheel();

    }

    public void TorqueWheel()
    {
        Vector3 torDir = new Vector3(Input.GetAxis("Vertical") * torqueForce * Time.deltaTime, 0, 0);
        rb.AddRelativeTorque(torDir, ForceMode.Acceleration);
    }
    public void RotateDirection()
    {
        Vector3 rotDir = new Vector3(Input.GetAxis("Horizontal") * torqueForce * Time.deltaTime, 0, 0);
        //rb.AddTorque(rotDir, ForceMode.Acceleration, Space.World);
    }
}
