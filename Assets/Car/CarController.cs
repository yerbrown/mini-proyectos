﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public List<WheelCollider> wheelsJoint = new List<WheelCollider>();
    public List<WheelCollider> frontWheels = new List<WheelCollider>();
    public List<GameObject> wheels = new List<GameObject>();
    public GameObject wheel;
    public float anguloMax;
    public float motorForce;
    public List<Light> frontLights = new List<Light>();
    public Material emmisiveLight;

    public Vector3 massOffset;
    [Header("WheelColliderAttributtes")]
    public float suspensionDistance = 0.5f;
    public float stiffnessSideways = 1;

    private void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = GetComponent<Rigidbody>().centerOfMass + massOffset;
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        for (int i = 0; i < wheelsJoint.Count; i++)
        {
            wheelsJoint[i].motorTorque = Input.GetAxis("Vertical") * motorForce;
            wheelsJoint[i].suspensionDistance = suspensionDistance;
        }
        for (int i = 0; i < frontWheels.Count; i++)
        {
            frontWheels[i].steerAngle = Input.GetAxis("Horizontal") * anguloMax;
        }
        TransformWheels();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.position = new Vector3(transform.position.x, 5, transform.position.z);
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
            GetComponent<Rigidbody>().Sleep();
            GetComponent<Rigidbody>().WakeUp();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (frontLights[0].enabled)
            {
                emmisiveLight.DisableKeyword("_EMISSION");
            }
            else
            {
                emmisiveLight.EnableKeyword("_EMISSION");
            }
            for (int i = 0; i < frontLights.Count; i++)
            {
                frontLights[i].enabled = !frontLights[i].enabled;
            }
        }


    }

    public void TransformWheels()
    {
        for (int i = 0; i < wheels.Count; i++)
        {
            Vector3 pos;
            Quaternion rot;
            wheelsJoint[i].GetWorldPose(out pos, out rot);
            wheels[i].transform.position = pos;
            wheels[i].transform.rotation = rot;
        }
    }
}
