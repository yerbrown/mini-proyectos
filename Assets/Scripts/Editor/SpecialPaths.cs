﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class SpecialPaths
{
    //Añadir un nuevo elemento que se muestra en el menu que sale al hacer click derecho en un asset
    [MenuItem("Assets/Load Scene")]
    private static void LoadScene()
    {
        //En este caso, abririamos la escena seleccionada
        var selected = Selection.activeObject;
        EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(selected));
    }

    //Añadir un nuevo elemento a las opciones de crewacion de assets
    [MenuItem("Assets/Create/Add Configuration")]
    private static void AddConfig()
    {
        //Por ejemplo, podemos usarlo para crear nuevos ScriptableObjects sin por todos sub submenus
    }

    //Añadimos un nuevo elemento al menu contextual de los RigidBody (el que se abre con click derecho)
    [MenuItem("CONTEXT/Rigidbody/Nogravity")]
    private static void Nogravity()
    {

    }

    //Esta funcion va asociada a la siguiente
    [MenuItem("Assets/ProccessTexture")]
    private static void DoSomethingWithTexture()
    {
        Debug.Log("Es textura");
    }
    //Utilizamos la misma ruta que en el anterior, pero en este caso añadimos una nueva variable para controlar si esta o no habilitado
    [MenuItem("Assets / ProccessTexture", true)]
    private static bool NewMenuOptionValidation()
    {
        return Selection.activeObject.GetType() == typeof(Texture2D);
    }
}
