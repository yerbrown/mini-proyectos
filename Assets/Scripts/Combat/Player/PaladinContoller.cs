﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaladinContoller : MonoBehaviour
{
    public Animator anim;
    public float jumpForce=100;
    public BoxCollider swordCollider;
    void Update()
    {
        anim.SetFloat("Forward", Input.GetAxis("Vertical"));
        anim.SetFloat("Right", Input.GetAxis("Horizontal"));
        if (anim.GetBool("CanAttack"))
        {
            if (Input.GetButtonDown("Fire1"))
            {
                anim.SetTrigger("Attack");
                ToogleNextAttack();
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {
            anim.SetBool("Block", true);
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            anim.SetBool("Block", false);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            anim.SetBool("Agacharse", true);
        }
        else if (Input.GetKeyUp(KeyCode.C))
        {
            anim.SetBool("Agacharse", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("Jump");
           
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool("Run", true);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("Run", false);
        }
    }
    public void ToogleNextAttack()
    {
        anim.SetBool("CanAttack", !anim.GetBool("CanAttack"));
    }

    public void Jump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Force);
    }

    public void ToogleSword()
    {
        swordCollider.isTrigger = !swordCollider.isTrigger;
    }
}
